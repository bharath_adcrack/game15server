using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game15Server
{
    /// <summary>
    /// Right panel.
    /// </summary>
    public class RightPanel : NetworkBehaviour, 
                                IBeginDragHandler, 
                                IDragHandler,
                                IEndDragHandler
    {
        #region Private fields
        NetworkInputData _networkInputData;
        private Vector2 _initialDragPos = Vector2.zero;
        #endregion


        #region Public methods
        public float RotationY;
        #endregion

        #region Monobehaviour callbacks

        private void Awake()
        {
            _networkInputData = new NetworkInputData();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region IHandler callbacks

        public void OnBeginDrag(PointerEventData eventData)
        {
            _initialDragPos = eventData.position;
            
        }

        public void OnDrag(PointerEventData eventData)
        {
            var currentDragPos = eventData.position - eventData.pressPosition;
            NetworkInputData data = new NetworkInputData();
            if(currentDragPos.x > _initialDragPos.x)
            {
                data.PlayerRotation = currentDragPos.normalized;
                //Debug.Log($"{nameof(OnDrag)} : Right {currentDragPos.normalized} data {data.PlayerRotation}");

            }
            else
            {
                data.PlayerRotation = currentDragPos.normalized;
                //Debug.Log($"{nameof(OnDrag)} : Left {currentDragPos.normalized} data {data.PlayerRotation}");
            }
            _initialDragPos = currentDragPos;
            // data.PlayerRotationValue();
            
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            
            // throw new System.NotImplementedException();
        }

        #endregion

        #region Network behaviour callbacks
        
        #endregion

        #region Public Methods
        public void PlayerRotation()
        {

        }

        
        #endregion

    }
}


