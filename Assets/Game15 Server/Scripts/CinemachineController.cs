using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game15Server
{
    /// <summary>
    /// Cinemachine camera controller.
    /// </summary>
    public class CinemachineController : MonoBehaviour
    {
        #region Serialize private fields
        /// <summary>
        /// Cinemachine Virtual camera component.
        /// </summary>
        [SerializeField] CinemachineVirtualCamera _cinemachineVirtualCamera;
        [SerializeField] Transform _player;
        #endregion

        #region Private fields
        private bool _playerAttachedToCamera;
        #endregion

        #region Public fields
        public CinemachineVirtualCamera CinemachineVirtualCamera { get { return _cinemachineVirtualCamera; } set { _cinemachineVirtualCamera = value; } }
        
        public Transform Player { get { return _player; } set { _player = value; } }

        public Transform[] PlayersList;
        #endregion

        #region Monobehaviour callbacks
        // Start is called before the first frame update
        void Start()
        {
            
            // InvokeRepeating(nameof(AttachPlayerToCinemachineVirtualCamera), 0.01f, 0.1f);

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region 
        public void AttachPlayerToCinemachineVirtualCamera(Transform player)
        {
            if (!_playerAttachedToCamera)
            {
                // var player = GameObject.FindGameObjectWithTag("Player");
                PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();
                if (player != null && !playerMovement.CameraAttached)
                {
                    _cinemachineVirtualCamera.Follow = player.transform;
                    _player = player.transform;
                    // _cinemachineVirtualCamera.LookAt = player.transform;
                    playerMovement.CameraAttached = true;
                    _playerAttachedToCamera = true;
                    // Debug.Log($"{nameof(AttachPlayerToCinemachineVirtualCamera)} : {_playerAttachedToCamera} Player {player.transform}");
                }
            }
            

        }
        #endregion
    }
}


