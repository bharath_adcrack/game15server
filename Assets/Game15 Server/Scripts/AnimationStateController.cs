using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game15Server
{
    /// <summary>
    /// Animation state controller script
    /// </summary>
    public class AnimationStateController : NetworkBehaviour
    {
        #region Private Fields
        /// <summary>
        /// Animator
        /// </summary>
        private Animator _animator;
        /// <summary>
        /// 2D Vector.
        /// </summary>
        private float _moveX;
        private float _moveZ;

        /// <summary>
        /// Move Z animator hash
        /// </summary>
        private readonly int _moveXhash = Animator.StringToHash("Move X");
        private readonly int _moveZhash = Animator.StringToHash("Move Z");
        private readonly int _jumpHash = Animator.StringToHash("Jump");
        private readonly int _speedHash = Animator.StringToHash("Speed");
        #endregion

        #region Public Fields
        public Animator _Animator { get { return _animator; } set { _animator = value;  } }
        public int MoveZhash { get { return _moveZhash; } }
        public int MoveXhash { get { return _moveXhash; } }

        public int JumpHash { get { return _jumpHash;} }
        public int SpeedHash { get { return _speedHash;} }
        #endregion

        #region Monobehaviour callbacks
        // Start is called before the first frame update
        void Start()
        {
            // Look for Animator component in the child game object.
            _animator = GetComponentInChildren<Animator>();
            _Animator = _animator;
            Debug.Log($"{nameof(AnimationStateController)} script : Animator {_animator.name}");
        }
        #endregion
    }
}


