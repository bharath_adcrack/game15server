using Fusion;
using Fusion.Sockets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Game15Server
{
    /// <summary>
    /// Server game controller for instantiating the player by the server.
    /// </summary>
    public class ServerGameController : SimulationBehaviour, INetworkRunnerCallbacks
    {
        // Player prefab
        [SerializeField] private NetworkObject _player;

        // Player map dictionary.
        private readonly Dictionary<PlayerRef, NetworkObject> _playerMap = new Dictionary<PlayerRef, NetworkObject>();


        #region Monobehaviour callbacks
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region INetwork Runner callbacks
        public void OnConnectedToServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
        {
            // throw new NotImplementedException();
        }

        public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data)
        {
            //throw new NotImplementedException();
        }

        public void OnDisconnectedFromServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
        {
            //throw new NotImplementedException();
        }

        public void OnInput(NetworkRunner runner, NetworkInput input)
        {
            //throw new NotImplementedException();
        }

        public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
        {
            //throw new NotImplementedException();
        }

        public void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
        {
            
            Vector3 randomPos = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));
            // Vector3 randomPos = new Vector3(0, 0, 0);
            var character = runner.Spawn(_player, randomPos, Quaternion.identity ,inputAuthority: player);

            _playerMap[player] = character;
            Debug.Log($"Character {character}");
         
        }

        public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
        {
            if (_playerMap.TryGetValue(player, out var character))
            {
                // Despawn Player
                runner.Despawn(character);

                // Remove player from mapping
                _playerMap.Remove(player);

                Log.Info($"Despawn for Player: {player}");
            }

            if (_playerMap.Count == 0)
            {
                Log.Info("Last player left, shutdown...");
                // Shutdown Server after the last player leaves
                // runner.Shutdown();
            }
            //throw new NotImplementedException();
        }

        public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadDone(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadStart(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
        {
            //throw new NotImplementedException();
        }

        public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
        {
            //throw new NotImplementedException();
        }

        public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
        {
            //throw new NotImplementedException();
        }
        #endregion
    }
}


